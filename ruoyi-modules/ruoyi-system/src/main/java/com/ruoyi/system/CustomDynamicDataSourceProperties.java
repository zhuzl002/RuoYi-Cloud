package com.ruoyi.system;

import com.alibaba.nacos.api.config.ConfigType;
import com.alibaba.nacos.api.config.annotation.NacosConfigurationProperties;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceProperties;
import lombok.Data;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

//@Primary
//@Component
//@Data
//@NacosConfigurationProperties(dataId = NacosYmlConfig.DATA_ID,prefix = DynamicDataSourceProperties.PREFIX,   autoRefreshed = true,
//        type = ConfigType.YAML)
public class CustomDynamicDataSourceProperties extends DynamicDataSourceProperties {
}
