package com.ruoyi.system;

import com.alibaba.cloud.nacos.NacosConfigManager;
import com.alibaba.nacos.api.config.ConfigChangeItem;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.client.config.impl.YmlChangeParser;
import com.baomidou.dynamic.datasource.DynamicRoutingDataSource;
import com.baomidou.dynamic.datasource.creator.DefaultDataSourceCreator;
import com.baomidou.dynamic.datasource.provider.DynamicDataSourceProvider;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * 监听nacos配置变化，可以在这动态新增数据源，无需重新启动项目
 */
//@Component
//@Data
@Slf4j
public class NacosYmlConfig {

    /**
     * Nacos dataId.
     */
    public static final String DATA_ID = "ruoyi-system-dev.yml";

    /**
     * Nacos group.
     */
    public static final String GROUP = "DEFAULT_GROUP";

    @Autowired
    private NacosConfigManager nacosConfigManager;
    @Resource
    private DefaultDataSourceCreator defaultDataSourceCreator;
    @Autowired
    private DataSource dataSource;
    @Resource
    private DynamicDataSourceProvider dynamicDataSourceProvider;


    private String beforContent="";

    @PostConstruct
    public void init() throws NacosException {
        ConfigService configService = nacosConfigManager.getConfigService();
        beforContent = configService.getConfig(DATA_ID,null,3000);
        configService.addListener(DATA_ID, GROUP, new Listener() {
            @Override
            public Executor getExecutor() {
                return Executors.newSingleThreadExecutor();
            }

            @Override
            public void receiveConfigInfo(String configInfo) {

                // 获取本次变更的数据项
                Map<String, ConfigChangeItem> stringConfigChangeItemMap = new YmlChangeParser().doParse(beforContent, configInfo, null);
                // 对比获取新增的数据源添加數據源
                DynamicRoutingDataSource ds = (DynamicRoutingDataSource) NacosYmlConfig.this.dataSource;
                DataSourceProperty dataSourceProperty = new DataSourceProperty();
                // 创建新增加的数据源
                //  DataSource addDataSource = defaultDataSourceCreator.createDataSource(dataSourceProperty);
                // 新增数据源
                // ds.addDataSource("dataSourceKey",addDataSource);

                //YmlDynamicDataSourceProvider
                log.info("[dataId]:[" + DATA_ID + "],Configuration changed to:"
                        + configInfo);
                log.info("[dataId]:[" + DATA_ID + "],Configuration changed content:"
                        + stringConfigChangeItemMap);
                beforContent = configInfo;
            }
        });
    }

}
