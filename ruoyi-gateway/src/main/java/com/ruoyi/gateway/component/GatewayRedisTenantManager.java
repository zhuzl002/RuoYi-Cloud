package com.ruoyi.gateway.component;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.core.event.DefaultMuiltTenantListener;
import com.ruoyi.common.core.event.MultiTenantEvent;
import com.ruoyi.common.core.event.SysTenantDTO;
import com.ruoyi.common.redis.component.RedisTemplateManager;
import com.ruoyi.common.redis.domain.RedisInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@Slf4j
public class GatewayRedisTenantManager extends DefaultMuiltTenantListener{

    @Resource
    private RedisTemplateManager redisTemplateManager;

    @Override
    public void onApplicationEvent(MultiTenantEvent event) {

        super.onApplicationEvent(event);
        SysTenantDTO tenant = (SysTenantDTO)event.getSource();
        RedisInfo redisInfo = JSON.parseObject(tenant.getRedisDataSource(), RedisInfo.class);
        redisTemplateManager.dynamicRedisTemplate(tenant.getTenantId(),redisInfo);
        log.info("refresh redis success! after:[{}]", tenant);
    }
}
