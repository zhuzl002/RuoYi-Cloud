package com.ruoyi.common.cs.domain;


import lombok.*;
import org.apache.http.HttpHost;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class EsClientInfo {

    private int connectTimeoutMillis = 1000;
    private int socketTimeoutMillis = 30000;
    private final List<HttpHost> httpHosts = new ArrayList<>();
    private String userName;
    private String password;
}
