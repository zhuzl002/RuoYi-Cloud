package com.ruoyi.common.cs.cache;

import com.ruoyi.common.core.context.SecurityContextHolder;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

public class ElasticsearchTemplateHoulder {

    /**
     * 默认，如果租户没有映射关系时使用
     */
    private static final String DEFAULT = "default_es";

    private static final String EMPTY = "";
    private static final Map<String, ElasticsearchRestTemplate> ES_TEMPLATE_MAP = new HashMap<>();

    @PostConstruct
    void init(ElasticsearchRestTemplate elasticsearchRestTemplate) {
        initDefaultEs(elasticsearchRestTemplate);
    }

    private static void initDefaultEs(ElasticsearchRestTemplate elasticsearchRestTemplate) {
        refreshTemplate(DEFAULT, elasticsearchRestTemplate);
    }

    public static ElasticsearchRestTemplate getEsTemplate(String tenantId) {
        return ES_TEMPLATE_MAP.getOrDefault(tenantId, ES_TEMPLATE_MAP.get(DEFAULT));
    }

    public static ElasticsearchRestTemplate getEsTemplate() {
        String tenantId = SecurityContextHolder.getTenantId();
        return getEsTemplate(tenantId);
    }

    public static synchronized void refreshTemplate(String tenantId, ElasticsearchRestTemplate elasticsearchRestTemplate) {
        ES_TEMPLATE_MAP.put(tenantId, elasticsearchRestTemplate);
    }
}
