package com.ruoyi.common.redis.aspect;

import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.utils.JwtUtils;
import com.ruoyi.common.core.utils.ServletUtils;
import com.ruoyi.common.core.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Aspect
@Component
public class RedisAspect  implements Ordered {

    /**
     * 拦截所有redis public 方法
     */
    @Pointcut("execution(public * com.ruoyi.common.redis.service..*.*(..))")
    public void serviceMethodPointcut() {
    }

    /**
     * 环绕切面 根据租户切换redis
     *
     * @param point /
     * @return Object  /
     * @throws Throwable /
     */
    @Around("serviceMethodPointcut()")
    public Object serviceAround(ProceedingJoinPoint point) throws Throwable {
        String shortName = point.getSignature().toShortString();
        HttpServletRequest request = ServletUtils.getRequest();
        String userid = null;
        String username = null;
        String tenantId = null;

//        log.info("SecurityUtils[{}][{}][{}]",SecurityUtils.getUserId(),SecurityUtils.getUsername(),SecurityUtils.getTenantId());
//        SecurityUtils.getToken();
        if(request != null ){
            if (StringUtils.isEmpty(userid)) {
                userid = ServletUtils.getHeader(request, SecurityConstants.DETAILS_USER_ID);
            }

            if (StringUtils.isEmpty(username)) {
                username = ServletUtils.getHeader(request, SecurityConstants.DETAILS_USERNAME);
            }

            if (StringUtils.isEmpty(tenantId)) {
                tenantId = ServletUtils.getHeader(request, SecurityConstants.TENANT_ID);
            }
            if (StringUtils.isEmpty(tenantId)) {
                // 尝试从auth获取
                tenantId = ServletUtils.getHeader(request, SecurityConstants.AUTH_TENANT_ID);
            }
        }
        String tempTenantId = MDC.get(SecurityConstants.TENANT_ID);
        if (StringUtils.isNotEmpty(tempTenantId)) {
            tenantId = tempTenantId;
        }

        String authTenantId = MDC.get(SecurityConstants.AUTH_TENANT_ID);
        if (StringUtils.isNotEmpty(authTenantId)) {
            tenantId = authTenantId;
        }

        String redisTenantId = MDC.get(SecurityConstants.REDIS_TENANT_ID);
        if (StringUtils.isNotEmpty(authTenantId) && StringUtils.isEmpty(tenantId)) {
            tenantId = redisTenantId;
        }

        Object result = null;
        try {
            MDC.put(SecurityConstants.REDIS_TENANT_ID,tenantId);
            log.info("[RedisAspect] userId:[{}],userName:[{}],tenantId:[{}]",userid,username,tenantId);
            result = point.proceed();
        } catch (Exception e) {
            result = e;
        } finally {
            MDC.remove(SecurityConstants.REDIS_TENANT_ID);
        }
        return result;
    }


    /**
     * 确保在权限认证aop执行前执行
     */
    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE + 1;
    }
}
