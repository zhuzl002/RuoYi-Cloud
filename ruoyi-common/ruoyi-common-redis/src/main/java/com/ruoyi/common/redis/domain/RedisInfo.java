package com.ruoyi.common.redis.domain;

import lombok.*;

@Data
@EqualsAndHashCode
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RedisInfo {

    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 6379;

    private String hostName = DEFAULT_HOST;
    private int port = DEFAULT_PORT;
    private int database;
    private String username = null;
    private String password;
    private String tenantId;
}
