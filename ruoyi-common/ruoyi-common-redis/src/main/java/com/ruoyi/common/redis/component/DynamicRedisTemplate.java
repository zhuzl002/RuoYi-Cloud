package com.ruoyi.common.redis.component;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.lettuce.LettuceConnection;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * 实现redisTemplate 自动切换
 * @param <K>
 * @param <V>
 */
@Slf4j
public class DynamicRedisTemplate<K,V> extends RedisTemplate<K,V> {

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
    }

    @Override
    protected RedisConnection createRedisConnectionProxy(RedisConnection connection) {
        return super.createRedisConnectionProxy(connection);
    }

    @Override
    protected RedisConnection preProcessConnection(RedisConnection connection, boolean existingConnection) {
        log.info("验证redis是否正常被切换,dbIndex:[{}]");
        return super.preProcessConnection(connection, existingConnection);
    }
}
