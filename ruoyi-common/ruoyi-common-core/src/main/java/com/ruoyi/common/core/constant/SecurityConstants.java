package com.ruoyi.common.core.constant;

/**
 * 权限相关通用常量
 * 
 * @author ruoyi
 */
public class SecurityConstants
{
    /**
     * 用户ID字段
     */
    public static final String DETAILS_USER_ID = "user_id";

    /**
     * 用户名字段
     */
    public static final String DETAILS_USERNAME = "username";

    /**
     * 授权信息字段
     */
    public static final String AUTHORIZATION_HEADER = "authorization";

    /**
     * 请求来源
     */
    public static final String FROM_SOURCE = "from-source";

    /**
     * 内部请求
     */
    public static final String INNER = "inner";

    /**
     * 用户标识
     */
    public static final String USER_KEY = "user_key";

    /**
     * 登录用户
     */
    public static final String LOGIN_USER = "login_user";

    /**
     * 角色权限
     */
    public static final String ROLE_PERMISSION = "role_permission";

    /**
     * 租户ID
     */
    public static final String TENANT_ID = "tenantId";

    /**
     * AUTH 租户ID,用于 auth 手动传递租户
     */
    public static final String AUTH_TENANT_ID = "authTenantId";

    public static final String TRACE_ID = "traceId";

    public static final String REDIS_TENANT_ID = "redisTenantId";

}
