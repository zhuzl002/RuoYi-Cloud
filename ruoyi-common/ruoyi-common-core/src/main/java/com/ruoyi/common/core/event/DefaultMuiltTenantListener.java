package com.ruoyi.common.core.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class DefaultMuiltTenantListener implements ApplicationListener<MultiTenantEvent> {

    private final Logger logger = LoggerFactory.getLogger(DefaultMultiTenantPublish.class);

    @Override
    public void onApplicationEvent(MultiTenantEvent event) {

        logger.info("[DefaultMuiltTenantListener] receive multiTenantEvent starting....");

        logger.info("[DefaultMuiltTenantListener] receive multiTenantEvent end....");
    }
}
