package com.ruoyi.common.core.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class DefaultMultiTenantPublish implements MultiTenantPublish {

    private final Logger logger = LoggerFactory.getLogger(DefaultMultiTenantPublish.class);

    private final ApplicationContext applicationContext;

    public DefaultMultiTenantPublish(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public Boolean publishTenant(MultiTenantEvent multiTenantEvent) {
        logger.info("[DefaultMultiTenantPublish] publishTenant: starting....");
        try {
            applicationContext.publishEvent(multiTenantEvent);
        }catch (Exception e){
            logger.error("[DefaultMultiTenantPublish] publishTenant: error,errorMsg:[{}]",e.getMessage(),e);
        return Boolean.FALSE;
        }
        logger.info("[DefaultMultiTenantPublish] publishTenant: end....");
        return Boolean.TRUE;
    }
}
