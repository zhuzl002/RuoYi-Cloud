package com.ruoyi.common.core.event;

public interface MultiTenantPublish {

    Boolean publishTenant(MultiTenantEvent multiTenantEvent);
}
