package com.ruoyi.common.core.event;

import com.ruoyi.common.core.enums.MultiTenantEnums;
import org.springframework.context.ApplicationEvent;

public class MultiTenantEvent extends ApplicationEvent {

    private String tenantId;

    private MultiTenantEnums multiTenantEnums;

    public MultiTenantEvent(MultiTenantEnums multiTenantEnums,String tenantId,SysTenantDTO source) {
        super(source);
        this.multiTenantEnums = multiTenantEnums;
        this.tenantId = tenantId;
    }

    public MultiTenantEnums getMultiTenantEnums() {
        return multiTenantEnums;
    }

    public String getTenantId() {
        return tenantId;
    }

    @Override
    public String toString() {
        return "MultiTenantEvent{" +
                "tenantId='" + tenantId + '\'' +
                ", multiTenantEnums=" + multiTenantEnums +
                ", source=" + source +
                '}';
    }
}
