package com.ruoyi.common.core.event;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 租户对象 sys_tenant
 *
 * @author ruoyi
 * @date 2022-10-13
 */
public class SysTenantDTO extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    private Long id;

    /**
     * 帐号状态（0正常 1停用）
     */
    private Integer status;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 租户名称
     */
    private String tenantName;

    /**
     * 数据源内容
     */
    private String dataSource;

    /**
     * redis数据源内容
     */
    private String redisDataSource;


    /**
     * es数据源内容
     */
    private String esDataSource;

    /**
     * 数据源key
     */
    private String dataSourceKey;

    /**
     * 数据源内容md5
     */
    private String md5;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantName(String tenantName) {
        this.tenantName = tenantName;
    }

    public String getTenantName() {
        return tenantName;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSourceKey(String dataSourceKey) {
        this.dataSourceKey = dataSourceKey;
    }

    public String getDataSourceKey() {
        return dataSourceKey;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getMd5() {
        return md5;
    }

    public String getRedisDataSource() {
        return redisDataSource;
    }

    public void setRedisDataSource(String redisDataSource) {
        this.redisDataSource = redisDataSource;
    }

    public String getEsDataSource() {
        return esDataSource;
    }

    public void setEsDataSource(String esDataSource) {
        this.esDataSource = esDataSource;
    }

    @Override
    public String toString() {
        return "SysTenantDTO{" +
                "id=" + id +
                ", status=" + status +
                ", tenantId='" + tenantId + '\'' +
                ", tenantName='" + tenantName + '\'' +
                ", dataSource='" + dataSource + '\'' +
                ", redisDataSource='" + redisDataSource + '\'' +
                ", esDataSource='" + esDataSource + '\'' +
                ", dataSourceKey='" + dataSourceKey + '\'' +
                ", md5='" + md5 + '\'' +
                '}';
    }
}
