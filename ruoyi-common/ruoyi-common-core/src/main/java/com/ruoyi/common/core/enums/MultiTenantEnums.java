package com.ruoyi.common.core.enums;

public enum MultiTenantEnums {
    /**
     * 新增租戶
     */
    INSERT("insert","新增租戶"),

    UPDATE("update","更新租戶"),
    ;

    private final String code;

    private final String desc;

    MultiTenantEnums(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
