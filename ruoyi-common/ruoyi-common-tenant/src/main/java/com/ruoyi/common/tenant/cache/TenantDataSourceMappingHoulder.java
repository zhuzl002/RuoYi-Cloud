package com.ruoyi.common.tenant.cache;

import com.ruoyi.common.tenant.domain.SysTenant;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 租户 数据源 映射关系，后续需要维护在表里
 */
public final class TenantDataSourceMappingHoulder {

    /**
     * 默认数据源，如果租户没有映射关系时使用
     */
    private static final String DEFAULT_DATA_SOURCE = "master";

    private static final String EMPTY = "";

    /**
     * 租户 数据源 映射关系
     * KEY: tenantId value: dataSource
     * eg:
     * tenant_id_1 -> master
     * tenant_id_2 -> slave
     */
    private static final Map<String, SysTenant> map = new HashMap<>();


    /**
     * 根据租户ID 获取数据源
     *
     * @param tenantId 租户ID
     * @return dataSourceKey 数据源KEY
     */
    public static String getDataSourceKey(String tenantId) {

        if (isEmpty(tenantId)) {
            tenantId = EMPTY;
        }
        if (map.containsKey(tenantId)) {
            return map.get(tenantId).getDataSourceKey();
        }

        return DEFAULT_DATA_SOURCE;
    }

    public static SysTenant getStsTenant(String tenantId) {

        if (isEmpty(tenantId)) {
            tenantId = EMPTY;
        }
        if (map.containsKey(tenantId)) {
            return map.get(tenantId);
        }

        return null;
    }

    /**
     * 新增映射关系
     *
     * @param tenantId      租户ID
     * @param sysTenant 数据源
     */
    public static void addMapping(String tenantId, SysTenant sysTenant) {
        map.put(tenantId, sysTenant);
    }

    public static boolean containsTenantId(String tenantId) {
        return map.containsKey(tenantId);
    }

    private static boolean isEmpty(Object object) {
        if (object == null) {
            return true;
        } else if (object instanceof CharSequence) {
            return ((CharSequence) object).length() == 0;
        } else if (object.getClass().isArray()) {
            return Array.getLength(object) == 0;
        } else if (object instanceof Collection) {
            return ((Collection) object).isEmpty();
        } else {
            return object instanceof Map ? ((Map) object).isEmpty() : false;
        }
    }

    private TenantDataSourceMappingHoulder() {
    }
}
