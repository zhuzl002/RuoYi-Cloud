package com.ruoyi.common.tenant.config;


import com.ruoyi.common.security.feign.FeignAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(FeignAutoConfiguration.class)
//@ConditionalOnMissingBean(FeignAutoConfiguration.class)
@ConditionalOnProperty(prefix = "tenant",name = "feign",havingValue = "true")
public class FeignCongiruation {
}
