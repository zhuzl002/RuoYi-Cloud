package com.ruoyi.common.tenant.domain;

import lombok.*;

@Data
@EqualsAndHashCode
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataSourceInfo {


    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * url
     * eg: jdbc:mysql://host:ip/ry-cloud-1?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8
     */
    private String url;

    /**
     * 驱动
     */
    private String driverClassName;

}
