package com.ruoyi.common.security.config;

import com.ruoyi.common.security.interceptor.TenantIdInterceptor;
import com.ruoyi.common.security.interceptor.TraceIdInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.ruoyi.common.security.interceptor.HeaderInterceptor;

/**
 * 拦截器配置
 *
 * @author ruoyi
 */
public class WebMvcConfig implements WebMvcConfigurer
{
    /** 不需要拦截地址 */
    public static final String[] excludeUrls = { "/login", "/logout", "/refresh" };

    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(getHeaderInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns(excludeUrls)
                .order(-10);

        registry.addInterceptor(new TenantIdInterceptor())
                .addPathPatterns("/**")
                .order(-11);

        registry.addInterceptor(new TraceIdInterceptor())
                .addPathPatterns("/**")
                .order(-11);
    }

    /**
     * 自定义请求头拦截器
     */
    public HeaderInterceptor getHeaderInterceptor()
    {
        return new HeaderInterceptor();
    }
}
